const comment = require('../controller/comment_controller');

module.exports = router => {

    router.post('/comment/create', comment.create);
    router.post('/comment/reply', comment.reply);
    router.post('/comment/get/bypost', comment.getByPost);

}
