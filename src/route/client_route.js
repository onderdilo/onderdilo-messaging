const client = require('../controller/client_controller');

module.exports = router => {

    router.post('/client/create', client.create);

}