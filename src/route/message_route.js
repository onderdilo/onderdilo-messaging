const message = require('../controller/message_controller');

module.exports = router => {

    router.post('/message/create', message.create);
    router.post('/message/get/all', message.getAll);
    router.post('/message/get/unread', message.getUnread);
    router.post('/message/get/inbox', message.getInbox);
    router.post('/message/get/outbox', message.getOutbox);
    router.post('/message/get/detail', message.getDetail);

}
