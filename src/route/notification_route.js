const notification = require('../controller/notification_controller');

module.exports = router => {

    router.post('/notification/create', notification.createOne);
    router.post('/notification/broadcast', notification.createMulti);
    router.post('/notification/get/byuser', notification.getByUser);
    router.post('/notification/set/read', notification.setRead);
    router.post('/notification/delete', notification.delete);

}