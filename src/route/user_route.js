const user = require('../controller/user_controller');

module.exports = router => {

    router.post('/user/update', user.update);

}
