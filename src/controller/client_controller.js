const Client = require('../model/client_model');
const jwt = require('jsonwebtoken');
const appConfig = require('../../config/app_config');
const secret = require('rand-token');

module.exports.create = (req, res, next) => {

    const name = req.body.name;
    // const secret = req.body.secret;

    if (!name) {
        let err = new Error('bad request');
        err.status = 400;
        return next(err);
    }

    Client.create({name: name, secret: secret.generate(32)}, (err, result) => {

        if (err) return next(err);

        return res.json({status: 200, message: 'success create client', result: result});

    });

};

module.exports.getToken = (req, res, next) => {

    const key = req.body.key;
    const secret = req.body.secret;

    if (!key || !secret) {
        let err = Error('bad request');
        err.status = 400;

        return next(err);
    }

    Client.findOne({_id: key, secret: secret}).exec((err, data) => {
        if (err) return next(err);

        if (!data) {
            let err = Error('data not found');
            return next(err);
        }

        let exp = Math.floor(Date.now() / 1000) + (60 * 60);

        let payload = {
            exp: exp,
            data: data
        };

        let refresh_payload = {
            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 336),
            data: data
        };

        let result = {};

        result.token = jwt.sign(payload, appConfig.jwt_secret);
        result.refresh_token = jwt.sign(refresh_payload, Buffer(appConfig.jwt_secret, 'base64'));
        result.exp = exp;

        return res.json({status: 200, message: 'success get token', result: result});
    });
};

module.exports.getRefreshToken = (req, res, next) => {

    const token = req.body.token;

    jwt.verify(token, Buffer(appConfig.jwt_secret, 'base64'), function (err, decoded) {
        if (err) return next(err);

        let exp = Math.floor(Date.now() / 1000) + (60 * 60);

        let payload = {
            exp: exp,
            data: decoded.data
        };

        let refresh_payload = {
            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 336),
            data: decoded.data
        };

        let result = {};

        result.token = jwt.sign(payload, appConfig.jwt_secret);
        result.refresh_token = jwt.sign(refresh_payload, Buffer(appConfig.jwt_secret, 'base64'));
        result.exp = exp;

        return res.json({
            status: 200,
            message: 'success get refresh token',
            result: result
        });

    });

}