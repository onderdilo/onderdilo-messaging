const User = require('../model/user_model');

module.exports.update = (req, res, next) => {
    const client = req.user.data;
    const user = req.body.user;

    if (!user) {
        let err = new Error('bad request');
        err.status = 400;
    }

    User.findOneAndUpdate({
        client: client._id, user_id: user.user_id
    }, user, {
        upsert: true, new: true, setDefaultsOnInsert: true
    }, (err, result) => {
        if (err) return next(err);

        return res.json({status: 200, message: 'success update user', result: result});
    });
};