const User = require('../model/user_model');
const Message = require('../model/message_model');
const mongoose = require("mongoose");
const async = require("async");

module.exports.create = (req, res, next) => {

    const sender = req.body.sender;
    const recipient = req.body.recipient;
    const body = req.body.body;

    const client = req.user.data;

    if (!sender || !recipient || !body) {
        let err = new Error('bad request');
        err.status = 400;
        return next(err);
    }

    let newSender;
    let newRecipient;
    let message;

    async.series([
        cb => {
            User.findOneAndUpdate({
                client: client._id, user_id: sender.user_id
            }, sender, {
                upsert: true, new: true, setDefaultsOnInsert: true
            }, (err, result) => {

                newSender = result;

                cb();
            });
        },
        cb => {
            User.findOneAndUpdate({
                client: client._id, user_id: recipient.user_id
            }, recipient, {
                upsert: true, new: true, setDefaultsOnInsert: true
            }, (err, result) => {

                newRecipient = result;

                cb();
            });
        },
        cb => {
            Message.create({client: client._id, sender: newSender._id, recipient: newRecipient._id, body: body}, (err, result) => {
                if (err) return next(err);

                message = result;

                cb();
            });
        }
    ], (err) => {
        if (err) return next(err);

        Message.findOne(message).populate('sender').populate('recipient').exec((err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success create message', result: result});
        });

    });

};

module.exports.getAll = (req, res, next) => {

    const user_id = parseInt(req.body.user_id);
    const limit = parseInt(req.body.limit || 10);
    const offset = parseInt(req.body.offset || 0);
    const sort = req.body.sort;

    const client = req.user.data;

    let sorting = 1

    if (sort == 'asc') sorting = -1;

    let user;
    let messages;

    async.series([
        cb => {
            User.findOne({client: client._id, user_id: user_id}).exec((err, result) => {
                if (err) return next(err);
                if (!result) return emptyMessage();

                user = result;

                cb();
            });
        },
        cb => {
            Message.aggregate([
                {
                    "$match": {
                        "$or": [
                            {"sender": user._id},
                            {"recipient": user._id}
                        ],
                        "client": mongoose.Types.ObjectId(client._id),
                        "delete": {"$exists": false}
                    }
                },
                {
                    "$sort": {"date": sorting}
                },
                {
                    "$group": {
                        "_id": {
                            "$cond": [
                                {"$eq": ["$sender", user._id]}, "$recipient", "$sender"
                            ]
                        },
                        "object_id": {"$last": "$_id"},
                        "sender": {"$last": "$sender"},
                        "recipient": {"$last": "$recipient"},
                        "body": {"$last": "$body"},
                        "date": {"$last": "$date"},
                    }
                },
                {
                    "$skip": offset
                },
                {
                    "$limit": limit
                }
            ], (err, result) => {
                if (err) return next(err);
                if (!result) return emptyMessage();

                messages = result;

                cb();
            });
        }
    ], err => {
        if (err) return next(err);

        Message.populate(messages, [{path: '_id', model: 'User'}, {path: 'sender'}, {path: 'recipient'}], (err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success get message all', result: result});
        });
    });

    emptyMessage = () => {
        return res.json({status: 200, message: 'success get message all', result: {}});
    }
/*

    Message.find({$or: [{'sender.id': user_id}, {'recipient.id': user_id}], client: client._id})
        .sort({date: sorting})
        .skip(offset)
        .limit(limit)
        .exec((err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success get message all', result: result});
        });
*/

};

module.exports.getUnread = (req, res, next) => {

    const user_id = req.body.user_id;
    const sort = req.body.sort;

    const client = req.user.data;

    let sorting = 1

    if (sort == 'asc') sorting = -1;

    let user;
    let messages;

    async.series([
        cb => {
            User.findOne({client: client._id, user_id: user_id}).exec((err, result) => {
                if (err) return next(err);
                if (!result) return emptyUnread();

                user = result;

                cb();
            });
        },
        cb => {
            Message.aggregate([
                {
                    "$match": {
                        "$or": [
                            {"sender": user._id},
                            {"recipient": user._id}
                        ],
                        "client": mongoose.Types.ObjectId(client._id),
                        "delete": {"$exists": false}
                    }
                },
                {
                    "$sort": {"date": sorting}
                },
                {
                    "$group": {
                        "_id": {
                            "$cond": [
                                {"$eq": ["$sender", user._id]}, "$recipient", "$sender"
                            ]
                        },
                        "object_id": {"$last": "$_id"},
                        "sender": {"$last": "$sender"},
                        "recipient": {"$last": "$recipient"},
                        "body": {"$last": "$body"},
                        "date": {"$last": "$date"},
                    }
                }
            ], (err, result) => {
                if (err) return next(err);
                if (!result) return emptyUnread();

                messages = result;

                cb();
            });
        }
    ], err => {
        if (err) return next(err);

        Message.populate(messages, [{path: '_id', model: 'User'}, {path: 'sender'}, {path: 'recipient'}], (err, result) => {
            if (err) return next(err);

            let items = {};
            items.total = result.length;
            items.items = result;

            return res.json({status: 200, message: 'success get message all', result: items});
        });

    });

    emptyUnread = () => {

        let items = {};
        items.total = 0;
        items.items = {};

        return res.json({status: 200, message: 'success get message all', result: items});
    }

};

module.exports.getInbox = (req, res, next) => {

    const user_id = parseInt(req.body.user_id);
    const limit = parseInt(req.body.limit);
    const offset = parseInt(req.body.offset);
    const sort = req.body.sort;

    const client = req.user.data;

    if (!user_id) {
        let err = new Error('bad request');
        err.status = 400;
        return next(err);
    }

    let sorting = -1

    if (sort == 'asc') sorting = 1;

    Message.find({'recipient.id': user_id, client: client._id})
        .sort({date: sorting})
        .skip(offset)
        .limit(limit)
        .exec((err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success get message inbox', result: result});
        });

};

module.exports.getOutbox = (req, res, next) => {

    const user_id = parseInt(req.body.user_id);
    const limit = parseInt(req.body.limit);
    const offset = parseInt(req.body.offset);
    const sort = req.body.sort;

    const client = req.user.data;

    if (!user_id) {
        let err = new Error('bad request');
        err.status = 400;
        return next(err);
    }

    let sorting = -1

    if (sort == 'asc') sorting = 1;

    Message.find({'sender.id': user_id, client: client._id})
        .sort({date: sorting})
        .skip(offset)
        .limit(limit)
        .exec((err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success get message outbox', result: result});
        });

};

module.exports.getDetail = (req, res, next) => {

    const user_id = parseInt(req.body.user_id);
    const victim_id = parseInt(req.body.victim_id);
    const limit = parseInt(req.body.limit);
    const offset = parseInt(req.body.offset);
    const sort = req.body.sort;

    const client = req.user.data;

    if (!user_id || !victim_id) {
        let err = new Error('bad request');
        err.status = 400;
        return next(err);
    }

    let sorting = -1

    if (sort == 'asc') sorting = 1;

    let user;
    let victim;
    let messages;

    async.series([
        cb => {
            User.findOne({client: client._id, user_id: user_id}).exec((err, result) => {
                if (err) return next(err);

                user = result;

                cb();
            });
        },
        cb => {
            User.findOne({client: client._id, user_id: victim_id}).exec((err, result) => {
                if (err) return next(err);

                victim = result;

                cb();
            });
        },
        cb => {
            Message.find({
                    $or: [
                        {'sender': user._id, 'recipient': victim._id},
                        {'sender': victim._id, 'recipient': user._id}
                    ], client: client._id
            })
            .sort({date: sorting})
            .skip(offset)
            .limit(limit)
            .exec((err, result) => {
                if (err) return next(err);

                messages = result;

                cb();
            });
        }
    ], err => {
        if (err) return next(err);

        Message.populate(messages, [{path: 'sender'}, {path: 'recipient'}], (err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success get message detail', result: result});
        });
    });

};