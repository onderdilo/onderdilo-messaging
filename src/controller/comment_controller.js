const Comment = require('../model/comment_model');
const User = require('../model/user_model');
const async = require("async");

module.exports.create = (req, res, next) => {

    const post = req.body.post;
    const user_id = req.body.user_id;
    const body = req.body.body;

    const client = req.user.data;

    let user;
    let comment;

    if (!post || !user_id || !body) {
        let err = new Error('bad request');
        err.status = 400;
        return next(err);
    }

    async.series([
        cb => {
            User.findOne({client: client._id, user_id: user_id}).exec((err, result) => {
                if (err) return next(err);

                user = result;

                cb();
            });
        },
        cb => {
            Comment.create({client: client._id, post: post, user: user._id, body: body}, (err, result) => {
                if (err) return next(err);

                comment = result;

                cb();
            });
        }
    ], err => {
        if (err) return next(err);

        Comment.findOne(comment).populate('user').exec((err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success create comment', result: result});
        });

    });

};

module.exports.reply = (req, res, next) => {

    const post = req.body.post;
    const user_id = req.body.user_id;
    const body = req.body.body;
    const comment_id = req.body.comment_id;

    const client = req.user.data;

    let user;

    if (!post || !user_id || !body || !comment_id) {
        let err = new Error('bad request');
        err.status = 400;
        return next(err);
    }

    async.series([
        cb => {
            User.findOne({client: client._id, user_id: user_id}).exec((err, result) => {
                if (err) return next(err);

                user = result;

                cb();
            });
        }
    ], err => {
        if (err) return next(err);

        Comment.findOne({_id: comment_id}).exec((err, comment) => {
            if (err) return next(err);

            let child = new Comment({client: client._id, post: post, user: user._id, body: body});

            comment.childs.push(child);

            comment.save( (err, result) => {
                if (err) return next(err);

                return res.json({status: 200, message: 'success reply comment', result: child});
            });
        });

    });

};

module.exports.getByPost = (req, res, next) => {

    const post_id = parseInt(req.body.post_id);
    const limit = parseInt(req.body.limit);
    const offset = parseInt(req.body.offset);
    const sort = req.body.sort;

    const client = req.user.data;

    let sorting = -1

    if (sort == 'asc') sorting = 1;

    Comment.find({'post.id': post_id, client: client._id})
        .populate('user')
        .sort({date: sorting})
        .skip(offset)
        .limit(limit)
        .exec((err, result) => {
            if (err) return next(err);

            return res.json({status: 200, message: 'success get comment by post', result: result});
        });

};