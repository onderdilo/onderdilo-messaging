const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var NotificationSchema = new Schema({
    client: {
        type: Schema.Types.ObjectId,
        ref: "Client"
    },
    type: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    body: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    read: {
        type: Date
    },
    delete: {
        type: Date
    }
});

module.exports = mongoose.model('Notification', NotificationSchema);