const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
    client: {
        type: Schema.Types.ObjectId,
        ref: "Client"
    },
    user_id: {
        type: String,
        required: true
    },
    avatar: {
        type: String
    },
    email: {
        type: String
    },
    username: {
        type: String
    },
    fullname: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User', UserSchema);