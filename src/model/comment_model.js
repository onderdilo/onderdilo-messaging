const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var CommentSchema = new Schema({
    client: {
        type: Schema.Types.ObjectId,
        ref: "Client"
    },
    post: {
        type: Schema.Types.Mixed,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    body: {
        type: String
    },
    childs: {
        type: [CommentSchema]
    },
    date: {
        type: Date,
        default: Date.now
    },
    read: {
        type: Date
    },
    delete: {
        type: Date
    }
});

module.exports = mongoose.model('Comment', CommentSchema);