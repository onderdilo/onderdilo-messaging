const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const secret = require('rand-token').uid;

const ClientSchema = new Schema({
    name: {
        type: String,
        unique : true,
        required : true
    },
    secret: {
        type: String,
        unique : true,
        required : true,
        default: secret(32)
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Client', ClientSchema);