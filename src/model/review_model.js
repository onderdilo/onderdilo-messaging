const mongoose = require("mongoose");

const ReviewSchema = new mongoose.Schema({
    post: Schema.Types.Mixed,
    user: Schema.Types.Mixed,
    body: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Review', ReviewSchema);