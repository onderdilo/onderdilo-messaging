const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    client: {
        type: Schema.Types.ObjectId,
        ref: "Client",
        required: true
    },
    sender: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    recipient: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    body: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    read: {
        type: Date
    },
    delete: {
        type: Date
    }
});

module.exports = mongoose.model('Message', MessageSchema);