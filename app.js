const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const jwtMiddleware = require('express-jwt');

const appConfig = require('./config/app_config');
const dbConfig = require('./config/db_config');

const db = mongoose.connect(dbConfig.url);

const userRoute = require('./src/route/user_route');
const clientRoute = require('./src/route/client_route');
const commentRoute = require('./src/route/comment_route');
const messageRoute = require('./src/route/message_route');


const clientController = require('./src/controller/client_controller');

const app = express();
const router = express.Router();

// view engine setup
app.set('views', path.join(__dirname, 'src/view'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res, next) => {
    return res.json('Access Denied!');
});

app.use(jwtMiddleware({secret: appConfig.jwt_secret}).unless({path: ['/token', '/refresh_token', '/v1/client/create']}));

app.use('/token', clientController.getToken);
app.use('/refresh_token', clientController.getRefreshToken);

app.use('/v1', router);

userRoute(router);
clientRoute(router);
commentRoute(router);
messageRoute(router);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {

    // render the error page
    res.status(err.status || 500);

    return res.json({status: err.status || 500, message: err.message});
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.render('error');
});

module.exports = app;
